/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciotaulesexercici1.entitats;

/**
 *
 * @author EstherSanchez
 */
public class Assignatura {
      //PK de l'assignatura
    private int idAssignatura;
    private String nom;
    private String descripcio;

    public Assignatura(int idAssignatura, String nom, String descripcio) {
        this.idAssignatura = idAssignatura;
        this.nom = nom;
        this.descripcio = descripcio;
    }

    public int getIdAssignatura() {
        return idAssignatura;
    }

    public void setIdAssignatura(int idAssignatura) {
        this.idAssignatura = idAssignatura;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    @Override
    public String toString() {
        return "Assignatura{" + "idAssignatura=" + idAssignatura + ", nom=" + nom + ", descripcio=" + descripcio + '}';
    }

}
