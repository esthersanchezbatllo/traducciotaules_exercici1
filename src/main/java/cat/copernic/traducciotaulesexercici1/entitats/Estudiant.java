/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciotaulesexercici1.entitats;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author EstherSanchez
 */
public class Estudiant {
     //PK de l'estudiant (DNI)
    private String id;
    private String nom;
    private String email;
    
    // Grup al qual pertany l'estudiant
    // Al tenir obligació de que un estudiant estigui vinculat a un grup forcem que en el constructor es subministri el grup
    // Per un control més adient, s'hauria de verificar al constructor 
    // que l'estudiant tingui un grup assignat sempre, però li treu flexibilitat 
    // a la classe. 
    private Grup grup;
    
    
    //Opcionalment podem tenir una llista d'avaluacions que inclouen assignatures i notes
    private List<Avaluacio> avaluacions; 


    public Estudiant(String id, String nom, String email, Grup grup) {
        this.id = id;
        this.nom = nom;
        this.email = email;
        this.grup = grup;
        this.avaluacions = new ArrayList<>(); // Inicialitzar la llista

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Grup getGrup() {
        return grup;
    }

    public void setGrup(Grup grup) {
        this.grup = grup;
    }

    @Override
    public String toString() {
        return "Estudiant{" + "id=" + id + ", nom=" + nom + ", email=" + email + '}';
    }
    
    
   //Opcionalment
     public List<Avaluacio> getAvaluacions() {
        return avaluacions;
    }

    public void setAvaluacions(List<Avaluacio> avaluacions) {
        this.avaluacions = avaluacions;
    }

    // Afegir una nova avaluació
    public void afegirAvaluacio(Avaluacio avaluacio) {
        this.avaluacions.add(avaluacio);
    }

}
