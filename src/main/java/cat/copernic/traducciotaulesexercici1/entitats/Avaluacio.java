/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciotaulesexercici1.entitats;

import java.sql.Date;
import java.time.LocalDate;

/**
 *
 * @author EstherSanchez
 */
public class Avaluacio {
     // any de l'avaluació
    private int any;
    
    // nota
    private float nota;
    
    // data de l'acta d'avaluació
    private LocalDate  dataActaAvaluacio; 
    
    //estudiant avaluat
    private Estudiant estudiant;
    
    // assignatura avaluada
    private Assignatura assignatura;

    public Avaluacio(Estudiant estudiant, Assignatura assignatura, int any, float nota, LocalDate dataActaAvaluacio) {
        this.estudiant = estudiant;
        this.assignatura = assignatura;
        this.any = any;
        this.nota = nota;
        this.dataActaAvaluacio = dataActaAvaluacio;
    }

    public int getAny() {
        return any;
    }

    public void setAny(int any) {
        this.any = any;
    }

    public float getNota() {
        return nota;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }

  public LocalDate getDataActaAvaluacio() {
        return dataActaAvaluacio;
    }

    public void setDataActaAvaluacio(LocalDate dataActaAvaluacio) {
        this.dataActaAvaluacio = dataActaAvaluacio;
    }

    public Estudiant getEstudiant() {
        return estudiant;
    }

    public void setEstudiant(Estudiant estudiant) {
        this.estudiant = estudiant;
    }

    public Assignatura getAssignatura() {
        return assignatura;
    }

    public void setAssignatura(Assignatura assignatura) {
        this.assignatura = assignatura;
    }

    @Override
    public String toString() {
        return "Avaluacio{" + "any=" + any + ", nota=" + nota + ", dataActaAvaluacio=" + dataActaAvaluacio + ", estudiant=" + estudiant + ", assignatura=" + assignatura + '}';
    }

}
