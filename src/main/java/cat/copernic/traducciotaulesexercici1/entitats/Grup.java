/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cat.copernic.traducciotaulesexercici1.entitats;

/**
 *
 * @author EstherSanchez
 */
public class Grup {
    //PK del grup
    private String nom;
    private String aula;

    public Grup(String nom, String aula) {
        this.nom = nom;
        this.aula = aula;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    @Override
    public String toString() {
        return "Grup{" + "nom=" + nom + ", aula=" + aula + '}';
    }

}
