/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package cat.copernic.traducciotaulesexercici1;

import cat.copernic.traducciotaulesexercici1.entitats.*;
import java.time.LocalDate;

/**
 *
 * @author EstherSanchez
 */
public class TraduccioTaulesExercici1 {

    public static void main(String[] args) {
         // Crear un grup
        Grup grup = new Grup("Grup 1", "Aula A");

        // Crear un estudiant
        Estudiant estudiant = new Estudiant("123", "Joan", "joan@example.com", grup);

        // Crear assignatures
        Assignatura mat = new Assignatura(1, "Matemàtiques", "Càlcul diferencial");
        Assignatura fis = new Assignatura(2, "Física", "Mecànica clàssica");

        // Crear avaluacions amb data de l'acta
        Avaluacio avaluacio1 = new Avaluacio(estudiant, mat, 2023, 8.5f, LocalDate.of(2023, 6, 15));
        Avaluacio avaluacio2 = new Avaluacio(estudiant, fis, 2023, 7.2f, LocalDate.of(2023, 6, 16));

        // Afegir avaluacions a l'estudiant
        estudiant.afegirAvaluacio(avaluacio1);
        estudiant.afegirAvaluacio(avaluacio2);

        // Mostrar les avaluacions amb la data de l'acta
        for (Avaluacio avaluacio : estudiant.getAvaluacions()) {
            System.out.println("Assignatura: " + avaluacio.getAssignatura().getNom() +
                               ", Nota: " + avaluacio.getNota() +
                               ", Data Acta: " + avaluacio.getDataActaAvaluacio());
        }
    }
}
